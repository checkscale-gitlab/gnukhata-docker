FROM python:3.8.6-alpine3.11

MAINTAINER karthik <kskarthik@disroot.org> | Navin<navin@dff.org.in> | Ishan <imasdekar@dff.org.in> | Krishnakant Mane <kk@dff.org.in>

# GNUKhata Release
ENV GK_VERSION 7.0
ENV PGDATA /var/lib/postgresql/data
# installing required dependencies
RUN apk update && apk add --no-cache \
    postgresql \
    postgresql-dev \
    git \
    nginx \ 
    build-base \
    zlib-dev \
    jpeg-dev

# Create users & set ownerships
RUN adduser -u 105 -D gkadmin -G postgres && \
    adduser -D www-data -G www-data && \ 
    chown www-data:www-data "/var/lib/nginx" && \
    mkdir /run/postgresql && \
    chown postgres:postgres /run/postgresql/

#make necessary changes to file system and copy files.
RUN mkdir gnukhata

#first copy important scripts into /gnukhata
#then give read and execute permisions
COPY gkutil.sh startscript.sh supervisord.conf /gnukhata/

RUN chmod 755 /gnukhata/gkutil.sh && chmod 755 /gnukhata/startscript.sh && chmod 755 /gnukhata/supervisord.conf && \
    chown  gkadmin /gnukhata

#copy the configuration files for nginx since it is now installed.

#initialise postgresql and create the empty database for GNUKhata.

#final setup
#clone the git repository and then create tables in the databases

WORKDIR /gnukhata

# Download the respositories from their master branch
RUN git clone -b master https://gitlab.com/gnukhata/gkcore.git && \
    git clone -b master https://gitlab.com/gnukhata/gkwebapp.git

# checkout to latest version of applications && install pip dependencies 
RUN cd gkcore && \
    git checkout tags/V${GK_VERSION} && \
    pip install -r requirements.txt && \
    cd ..

RUN cd gkwebapp && \
    git checkout tags/${GK_VERSION} && \
    pip install -r requirements.txt && \
    cd ..

#copy the configuration files for nginx since it is now installed.

#initialise postgresql and create the empty database for GNUKhata.

COPY nginx.conf /etc/nginx/

COPY gnukhata.conf /etc/nginx/conf.d/

WORKDIR /gnukhata/gkcore

RUN python setup.py develop

# Postgres stuff

USER postgres
RUN mkdir -p ${PGDATA} && \ 
    chmod 0700 ${PGDATA} && \
    initdb  && \
    pg_ctl start && \ 
    sh /gnukhata/gkutil.sh &&\
    pg_ctl stop

USER root
RUN su -c 'pg_ctl start' postgres &&\
    su -c 'python3 initdb.py' gkadmin &&\
    su -c 'pg_ctl stop' postgres 

WORKDIR /gnukhata/gkwebapp

RUN python3 setup.py develop

WORKDIR /gnukhata

# Delete temp files
RUN rm -rf /tmp/* /var/tmp/* && rm /etc/nginx/conf.d/default.conf

EXPOSE 80

ENTRYPOINT /gnukhata/startscript.sh
